#ifndef DEV_CODE_CU_
#define DEV_CODE_CU_

#include <mma.h>
using namespace nvcuda;


template<typename T, typename U>
__global__ void _cgemm_kernel_v1(const T* a, const T* b, U* c, int k_size, int m_size, int n_size)
{
  typedef decltype(T::x) real;
  typedef decltype(T::y) imag;
  typedef decltype(U::x) o_real;
  typedef decltype(U::y) o_imag;

  int thrd_id = threadIdx.x;
  int warp_id = threadIdx.y; // Local warp id
  int a_row = blockIdx.x * TC_M + warp_id;
  int c_row = blockIdx.x * TC_M;
  int c_col = blockIdx.y * TC_N * N_WARPS + (warp_id % 4) * WARP_SIZE + thrd_id;


  T temp_a, temp_b;

  wmma::fragment<wmma::accumulator, TC_M, TC_N, TC_K, o_real> c_frag_r[N_WARPS];
  wmma::fragment<wmma::accumulator, TC_M, TC_N, TC_K, o_imag> c_frag_i[N_WARPS];
  wmma::fragment<wmma::matrix_a, TC_M, TC_N, TC_K, real, wmma::row_major> a_frag_r[(WARP_SIZE/TC_K)];
  wmma::fragment<wmma::matrix_a, TC_M, TC_N, TC_K, imag, wmma::row_major> a_frag_i[(WARP_SIZE/TC_K)];
  wmma::fragment<wmma::matrix_b, TC_M, TC_N, TC_K, real, wmma::row_major> b_frag_r[N_WARPS];
  wmma::fragment<wmma::matrix_b, TC_M, TC_N, TC_K, imag, wmma::row_major> b_frag_i[N_WARPS];

  // allocate shared memory
  __shared__ real smem_a_r[TC_M * WARP_SIZE];
  __shared__ imag smem_a_i[TC_M * WARP_SIZE];
  __shared__ real smem_b_r[TC_N * WARP_SIZE * N_WARPS];
  __shared__ imag smem_b_i[TC_N * WARP_SIZE * N_WARPS];
  __shared__ o_real smem_c_r[N_WARPS * TC_N * TC_M];
  __shared__ o_imag smem_c_i[N_WARPS * TC_N * TC_M];

  wmma::fill_fragment(c_frag_i[warp_id], 0);
  wmma::fill_fragment(c_frag_r[warp_id], 0);

  // if(a_row < m_size && (warp_id/2)*WARP_SIZE < n_size)
  // {
    for(int i = 0; i < k_size; i+=WARP_SIZE)
    {
      // Load 16 x 32 Tile from matrix A
      for(int ii = 0; ii < TC_M; ii += N_WARPS)
      {
          temp_a = a[(a_row + ii) * k_size + i + thrd_id]; // Matrix A is row major
          // Split imag and real part
          smem_a_r[(warp_id + ii) * WARP_SIZE + thrd_id] = temp_a.x;
          smem_a_i[(warp_id + ii) * WARP_SIZE + thrd_id] = temp_a.y;
      }
      // Load 8 times 16 x 32 matrices
      // for(int ii = 0; ii < TC_N; ii++)
      for(int ii = 0; ii < WARP_SIZE; ii+=2)
      {
        // Load data from global to register and unvectorize real and imaginary part
        temp_b = b[(i+ii+warp_id/4) * n_size + (warp_id%4) * WARP_SIZE + thrd_id]; // Matrix B is row major
        smem_b_r[(warp_id/4 + ii) * TC_N * N_WARPS + (warp_id%4) * WARP_SIZE + thrd_id] = temp_b.x;
        smem_b_i[(warp_id/4 + ii) * TC_N * N_WARPS + (warp_id%4) * WARP_SIZE + thrd_id] = temp_b.y;
      }
__syncthreads(); // Ensure data is loaded from global to shared memory
      int cnt = 0;
      for(int k = 0; k < WARP_SIZE; k+=TC_K)
      {
        // Load Tensor matrices
        wmma::load_matrix_sync(a_frag_r[cnt], &smem_a_r[k], WARP_SIZE);
        wmma::load_matrix_sync(a_frag_i[cnt], &smem_a_i[k], WARP_SIZE);
        // wmma::load_matrix_sync(b_frag_r[warp_id], &smem_b_r[warp_id * TC_N * WARP_SIZE + k], WARP_SIZE);
        // wmma::load_matrix_sync(b_frag_i[warp_id], &smem_b_i[warp_id * TC_N * WARP_SIZE + k], WARP_SIZE);
        wmma::load_matrix_sync(b_frag_r[warp_id], &smem_b_r[k * TC_N*N_WARPS + warp_id * TC_N], TC_N*N_WARPS);
        wmma::load_matrix_sync(b_frag_i[warp_id], &smem_b_i[k * TC_N*N_WARPS + warp_id * TC_N], TC_N*N_WARPS);
        // a_smem_real * b_smem_real -> real component
        wmma::mma_sync(c_frag_r[warp_id], a_frag_r[cnt], b_frag_r[warp_id], c_frag_r[warp_id]);
        // a_smem_real * b_smem_imag -> imaginary
        wmma::mma_sync(c_frag_i[warp_id], a_frag_r[cnt], b_frag_i[warp_id], c_frag_i[warp_id]);
        // a_smem_imag * b_smem_real -> imaginary
        wmma::mma_sync(c_frag_i[warp_id], a_frag_i[cnt], b_frag_r[warp_id], c_frag_i[warp_id]);
        // -a_smem_imag * b_smem_imag -> real
  #pragma unroll
        for(int t = 0; t < b_frag_i[warp_id].num_elements; t++)
        {
          b_frag_i[warp_id].x[t] *= -1;
        }

__syncthreads(); // Ensure all warps within a block are sync
        wmma::mma_sync(c_frag_r[warp_id], a_frag_i[cnt], b_frag_i[warp_id], c_frag_r[warp_id]);
        cnt++;
      }
    }

    wmma::store_matrix_sync(&smem_c_r[warp_id * TC_N], c_frag_r[warp_id], TC_N*N_WARPS, wmma::mem_row_major);
    wmma::store_matrix_sync(&smem_c_i[warp_id * TC_N], c_frag_i[warp_id], TC_N*N_WARPS, wmma::mem_row_major);

__syncthreads(); // Ensure all warps within a block are sync

    for(int i = 0; i < N_WARPS; i++)
    {
      if(warp_id < (N_WARPS/2))
      {
        c[(c_row+i*2) * n_size + c_col] = {
          smem_c_r[(i * N_WARPS + warp_id) * WARP_SIZE + thrd_id],
          smem_c_i[(i * N_WARPS + warp_id) * WARP_SIZE + thrd_id]
        };
      }
      else
      {
        c[(c_row+i*2+1) * n_size + c_col] = {
          smem_c_r[(i * N_WARPS + warp_id) * WARP_SIZE + thrd_id],
          smem_c_i[(i * N_WARPS + warp_id) * WARP_SIZE + thrd_id]
        };
      }
    }
  // }
}

template<typename T, typename U>
__global__ void _cgemm_kernel_v2(const T* a, const T* b, U* c, int k_size, int m_size, int n_size)
{
  typedef decltype(T::x) real;
  typedef decltype(T::y) imag;
  typedef decltype(U::x) o_real;
  typedef decltype(U::y) o_imag;

  int thrd_id = threadIdx.x;
  int warp_id = threadIdx.y; // Local warp id
  int a_row = blockIdx.x * TC_M + warp_id;
  int b_col = blockIdx.y * TC_N * N_WARPS + warp_id * TC_N;
  int c_row = blockIdx.x * TC_M;
  int c_col = blockIdx.y * TC_N * N_WARPS + (warp_id % 4) * WARP_SIZE + thrd_id;


  T temp_a, temp_b;

  wmma::fragment<wmma::accumulator, TC_M, TC_N, TC_K, o_real> c_frag_r[N_WARPS];
  wmma::fragment<wmma::accumulator, TC_M, TC_N, TC_K, o_imag> c_frag_i[N_WARPS];
  wmma::fragment<wmma::matrix_a, TC_M, TC_N, TC_K, real, wmma::row_major> a_frag_r[(WARP_SIZE/TC_K)];
  wmma::fragment<wmma::matrix_a, TC_M, TC_N, TC_K, imag, wmma::row_major> a_frag_i[(WARP_SIZE/TC_K)];
  wmma::fragment<wmma::matrix_b, TC_M, TC_N, TC_K, real, wmma::col_major> b_frag_r[N_WARPS];
  wmma::fragment<wmma::matrix_b, TC_M, TC_N, TC_K, imag, wmma::col_major> b_frag_i[N_WARPS];

  // allocate shared memory
  __shared__ real smem_a_r[TC_M * WARP_SIZE];
  __shared__ imag smem_a_i[TC_M * WARP_SIZE];
  __shared__ real smem_b_r[TC_N * WARP_SIZE * N_WARPS];
  __shared__ imag smem_b_i[TC_N * WARP_SIZE * N_WARPS];
  __shared__ o_real smem_c_r[N_WARPS * TC_N * TC_M];
  __shared__ o_imag smem_c_i[N_WARPS * TC_N * TC_M];

  wmma::fill_fragment(c_frag_i[warp_id], 0);
  wmma::fill_fragment(c_frag_r[warp_id], 0);

  // if(a_row < m_size && (warp_id/2)*WARP_SIZE < n_size)
  // {
    for(int i = 0; i < k_size; i+=WARP_SIZE)
    {
      // Load 16 x 32 Tile from matrix A
      for(int ii = 0; ii < TC_M; ii += N_WARPS)
      {
          temp_a = a[(a_row + ii) * k_size + i + thrd_id]; // Matrix A is row major
          // Split imag and real part
          smem_a_r[(warp_id + ii) * WARP_SIZE + thrd_id] = temp_a.x;
          smem_a_i[(warp_id + ii) * WARP_SIZE + thrd_id] = temp_a.y;
      }
      // Load 8 times 16 x 32 matrices
      // for(int ii = 0; ii < TC_N; ii++)
      for(int ii = 0; ii < WARP_SIZE; ii+=2)
      {
        // Load data from global to register and unvectorize real and imaginary part
        temp_b = b[(b_col + ii) * k_size + i + thrd_id]; // Matrix B is column major
        smem_b_r[(warp_id * TC_N + ii) * WARP_SIZE + thrd_id] = temp_b.x;
        smem_b_i[(warp_id * TC_N + ii) * WARP_SIZE + thrd_id] = temp_b.y;
      }
__syncthreads(); // Ensure data is loaded from global to shared memory
      int cnt = 0;
      for(int k = 0; k < WARP_SIZE; k+=TC_K)
      {
        // Load Tensor matrices
        wmma::load_matrix_sync(a_frag_r[cnt], &smem_a_r[k], WARP_SIZE);
        wmma::load_matrix_sync(a_frag_i[cnt], &smem_a_i[k], WARP_SIZE);
        wmma::load_matrix_sync(b_frag_r[warp_id], &smem_b_r[warp_id * TC_N * WARP_SIZE + k], WARP_SIZE);
        wmma::load_matrix_sync(b_frag_i[warp_id], &smem_b_i[warp_id * TC_N * WARP_SIZE + k], WARP_SIZE);
        // a_smem_real * b_smem_real -> real component
        wmma::mma_sync(c_frag_r[warp_id], a_frag_r[cnt], b_frag_r[warp_id], c_frag_r[warp_id]);
        // a_smem_real * b_smem_imag -> imaginary
        wmma::mma_sync(c_frag_i[warp_id], a_frag_r[cnt], b_frag_i[warp_id], c_frag_i[warp_id]);
        // a_smem_imag * b_smem_real -> imaginary
        wmma::mma_sync(c_frag_i[warp_id], a_frag_i[cnt], b_frag_r[warp_id], c_frag_i[warp_id]);
        // -a_smem_imag * b_smem_imag -> real
  #pragma unroll
        for(int t = 0; t < b_frag_i[warp_id].num_elements; t++)
        {
          b_frag_i[warp_id].x[t] *= -1;
        }

__syncthreads(); // Ensure all warps within a block are sync
        wmma::mma_sync(c_frag_r[warp_id], a_frag_i[cnt], b_frag_i[warp_id], c_frag_r[warp_id]);
        cnt++;
      }
    }

    wmma::store_matrix_sync(&smem_c_r[warp_id * TC_N], c_frag_r[warp_id], TC_N*N_WARPS, wmma::mem_row_major);
    wmma::store_matrix_sync(&smem_c_i[warp_id * TC_N], c_frag_i[warp_id], TC_N*N_WARPS, wmma::mem_row_major);

__syncthreads(); // Ensure all warps within a block are sync

    for(int i = 0; i < N_WARPS; i++)
    {
      if(warp_id < (N_WARPS/2))
      {
        c[(c_row+i*2) * n_size + c_col] = {
          smem_c_r[(i * N_WARPS + warp_id) * WARP_SIZE + thrd_id],
          smem_c_i[(i * N_WARPS + warp_id) * WARP_SIZE + thrd_id]
        };
      }
      else
      {
        c[(c_row+i*2+1) * n_size + c_col] = {
          smem_c_r[(i * N_WARPS + warp_id) * WARP_SIZE + thrd_id],
          smem_c_i[(i * N_WARPS + warp_id) * WARP_SIZE + thrd_id]
        };
      }
    }
  // }
}


// Function description in Beamformer.cuh
template<typename T, typename U>__global__
void _cgemm_kernel_v0(const T *a, const T* b, U *c, int k_size, int m_size, int n_size)
{
  const int thrd_id = threadIdx.x;
  const int warp_id = threadIdx.y;
  const int a_row = blockDim.x * blockIdx.x + warp_id;
  const int b_col = blockDim.y * blockIdx.y + thrd_id;

  U temp_c = {0,0};

  __shared__ T smem_a[WARP_SIZE][WARP_SIZE];
  __shared__ T smem_b[WARP_SIZE][WARP_SIZE];

  smem_a[warp_id][thrd_id] = {0,0};
  smem_b[warp_id][thrd_id] = {0,0};

  for( int k = 0; k < k_size; k+=WARP_SIZE )
  {
    const int a_col = k + thrd_id;
    const int b_row = k + warp_id;
    if(a_row < m_size && thrd_id + k < k_size)
    {
      smem_a[warp_id][thrd_id] = a[a_row * k_size + a_col];
    }
    else
    {
      smem_a[warp_id][thrd_id] = {0,0};
    }

    if(b_row < n_size && warp_id + k < k_size)
    {
      smem_b[warp_id][thrd_id] = b[b_row * n_size + b_col];
    }
    else
    {
      smem_b[warp_id][thrd_id] = {0,0};
    }

__syncthreads();

    for(int j = 0; j < WARP_SIZE; j++)
    {
      if(j + k == k_size)
      {
        break;
      }
      temp_c = cmadd(smem_a[warp_id][j], smem_b[j][thrd_id], temp_c);
    }
  }

__syncthreads();

  if(b_col < n_size && a_row < m_size)
  {
    c[a_row * n_size + b_col] = temp_c;
  }
}

__global__ void _cgemm_kernel_v1(const float2* a, const float2* b, float2* c, int k_size, int m_size, int n_size){}
__global__ void _cgemm_kernel_v1(const char2* a, const char2* b, float2* c, int k_size, int m_size, int n_size){}
__global__ void _cgemm_kernel_v2(const float2* a, const float2* b, float2* c, int k_size, int m_size, int n_size){}
__global__ void _cgemm_kernel_v2(const char2* a, const char2* b, float2* c, int k_size, int m_size, int n_size){}
__global__ void _cgemm_kernel_v0(const char2 *a, const char2* b, float2 *c, int k_size, int m_size, int n_size){}

#endif
