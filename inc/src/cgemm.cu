#ifdef CGEMM_CUH_

template<typename InputType, typename OutputType>
cgemm<InputType,OutputType>::cgemm(int k, int m, int n, int version)
: _k(k),_m(m),_n(n),_version(version)
{
  CUDA_ERROR_CHECK(cudaMalloc((void**)&a, k*m*sizeof(InputType)));
  CUDA_ERROR_CHECK(cudaMalloc((void**)&b, k*n*sizeof(InputType)));
  CUDA_ERROR_CHECK(cudaMalloc((void**)&c, m*n*sizeof(OutputType)));
  switch(_version)
  {
    case 0:
      block.x = WARP_SIZE;
      block.y = WARP_SIZE;
      grid.x = (int)m/WARP_SIZE;
      grid.y = (int)n/WARP_SIZE;
      grid.z = 1;
      break;
    case 1:
      block.x = WARP_SIZE;
      block.y = N_WARPS;
      grid.x = ceil((float)m/TC_M);
      grid.y = ceil((float)n/(N_WARPS * TC_N));
      grid.z = 1;
      break;
    case 2:
      block.x = WARP_SIZE;
      block.y = N_WARPS;
      grid.x = ceil((float)m/TC_M);
      grid.y = ceil((float)n/(N_WARPS * TC_N));
      grid.z = 1;
      break;
    case 3:
      if(std::is_same<InputType, cuComplex>::value){
        dtype = CUDA_C_32F;
      }else if(std::is_same<InputType, char2>::value){
        dtype = CUDA_C_8I;
      }else{
        std::cerr << "Data type not support by version 3";
        exit(1);
      }
      CUBLAS_ERROR_CHECK(cublasCreate(&blas_handle));
      break;
    default:
      std::cout << "Version " << _version << " not implemented" << std::endl;
  }
#ifdef DEBUG
  std::cout << "Initialized CGEMM instance with ("
    << k << " x " << m << " x " << n
    << ")\n  input type: " << typeid(a).name() << "; size: " << sizeof(InputType)
    << "\n  output type: " << typeid(c).name() << "; size: " << sizeof(OutputType) << std::endl;
#endif
}

template<typename InputType, typename OutputType>
cgemm<InputType,OutputType>::~cgemm()
{
switch(_version)
{
  case 0:
    break;
  case 1:
    break;
  case 2:
    break;
  case 3:
    CUBLAS_ERROR_CHECK(cublasDestroy(blas_handle));
    break;
  default:
    std::cout << "Version " << _version << " not implemented" << std::endl;
}

}


template<typename InputType, typename OutputType>
void cgemm<InputType,OutputType>::process()
{
  switch(_version)
  {
    case 0:
      _cgemm_kernel_v0<<<grid, block>>>(a, b, c, _k, _m, _n);
      break;
    case 1:
      _cgemm_kernel_v1<<<grid, block>>>(a, b, c, _k, _m, _n);
      break;
    case 2:
      _cgemm_kernel_v2<<<grid, block>>>(a, b, c, _k, _m, _n);
      break;
    case 3:
      cuComplex alpha, beta;
      alpha = {1,0};
      beta = {0,0};
      CUBLAS_ERROR_CHECK(cublasCgemmEx(blas_handle, CUBLAS_OP_N, CUBLAS_OP_N,
        _m, _n, _k, &alpha,
        (const void*)a, dtype, _m,
        (const void*)b, dtype, _k,
        &beta,
        (void*)c, CUDA_C_32F, _m
      ));
      CUDA_ERROR_CHECK(cudaDeviceSynchronize());
      break;
    default:
      return;
  }
}

template<typename InputType, typename OutputType>
void cgemm<InputType,OutputType>::klayout()
{
  std::cout << "grid.x: " << grid.x << std::endl;
  std::cout << "grid.y: " << grid.y << std::endl;
  std::cout << "grid.z: " << grid.z << std::endl;
  std::cout << "block.x: " << block.x << std::endl;
  std::cout << "block.y: " << block.y << std::endl;
  std::cout << "block.z: " << block.z << std::endl;
}

#endif
