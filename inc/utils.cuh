#ifndef UTILS_CUH_
#define UTILS_CUH_
#include <sstream>
#include <stdexcept>
#include <cuda.h>
#include <cublas_v2.h>

// __device__ float2 cmadd(__half2 a, __half2 b, float2 c={0,0})
// {
// 	float2 val;
// 	val.x = c.x + (float)(a.x * b.x - a.y * b.y);
// 	val.y = c.y + (float)(a.x * b.y + a.y * b.x);
// 	return val;
// }

__host__ __device__ float2 cmadd(__half2 a, __half2 b, float2 c={0,0})
{
	float2 val;
	float2 a1 = __half22float2(a);
	float2 b1 = __half22float2(b);
	val.x = c.x + (float)(a1.x * b1.x - a1.y * b1.y);
	val.y = c.y + (float)(a1.x * b1.y + a1.y * b1.x);
	return val;
}

__host__ __device__ float2 cmadd(float2 a, float2 b, float2 c={0,0})
{
	float2 val;
	val.x = c.x + (a.x * b.x - a.y * b.y);
	val.y = c.y + (a.x * b.y + a.y * b.x);
	return val;
}
__host__ __device__ int2 cmadd(char2 a, char2 b, int2 c={0,0})
{
	int2 val;
	val.x = c.x + (a.x * b.x - a.y * b.y);
	val.y = c.y + (a.x * b.y + a.y * b.x);
	return val;
}
__host__ __device__ float2 cmadd(char2 a, char2 b, float2 c={0,0})
{
	float2 val;
	val.x = c.x + (a.x * b.x - a.y * b.y);
	val.y = c.y + (a.x * b.y + a.y * b.x);
	return val;
}
__host__ __device__ int2 cmadd(int2 a, int2 b, int2 c={0,0})
{
	int2 val;
	val.x = c.x + (a.x * b.x - a.y * b.y);
	val.y = c.y + (a.x * b.y + a.y * b.x);
	return val;
}



#define CUDA_ERROR_CHECK(ans) { cuda_assert_success((ans), __FILE__, __LINE__); }

/**
 * @brief Function that raises an error on receipt of any cudaError_t
 *  value that is not cudaSuccess
 */
//inline void cuda_assert_success(cudaError_t code, const char *file, int line)
inline void cuda_assert_success(cudaError_t code, const char *file, int line)
{
    if (code != cudaSuccess)
    {
        std::stringstream error_msg;
        error_msg << "CUDA failed with error: "
              << cudaGetErrorString(code) << std::endl
              << "File: " << file << std::endl
              << "Line: " << line << std::endl;
        throw std::runtime_error(error_msg.str());
    }
}

#define CUBLAS_ERROR_CHECK(ans) { cublas_assert_success((ans), __FILE__, __LINE__); }

/**
 * @brief Function that raises an error on receipt of any cudaError_t
 *  value that is not cudaSuccess
 */
//inline void cuda_assert_success(cudaError_t code, const char *file, int line)
inline void cublas_assert_success(cublasStatus_t code, const char *file, int line)
{
	std::string error_code;
	std::stringstream error_msg;
	switch(code)
	{
		case CUBLAS_STATUS_SUCCESS:
			return;
		case CUBLAS_STATUS_NOT_INITIALIZED:
			error_code = "CUBLAS_STATUS_NOT_INITIALIZED";
			break;
		case CUBLAS_STATUS_ALLOC_FAILED:
			error_code = "CUBLAS_STATUS_ALLOC_FAILED";
			break;
		case CUBLAS_STATUS_INVALID_VALUE:
			error_code = "CUBLAS_STATUS_INVALID_VALUE";
			break;
		case CUBLAS_STATUS_ARCH_MISMATCH:
			error_code = "CUBLAS_STATUS_ARCH_MISMATCH";
			break;
		case CUBLAS_STATUS_MAPPING_ERROR:
			error_code = "CUBLAS_STATUS_MAPPING_ERROR";
			break;
		case CUBLAS_STATUS_EXECUTION_FAILED:
			error_code = "CUBLAS_STATUS_EXECUTION_FAILED";
			break;
		case CUBLAS_STATUS_INTERNAL_ERROR:
			error_code = "CUBLAS_STATUS_INTERNAL_ERROR";
			break;
		case CUBLAS_STATUS_NOT_SUPPORTED:
			error_code = "CUBLAS_STATUS_NOT_SUPPORTED";
			break;
		case CUBLAS_STATUS_LICENSE_ERROR:
			error_code = "CUBLAS_STATUS_LICENSE_ERROR";
			break;
	}
  error_msg << "CUDA failed with error: "
        << error_code << std::endl
        << "File: " << file << std::endl
        << "Line: " << line << std::endl;
  throw std::runtime_error(error_msg.str());
}


#endif
