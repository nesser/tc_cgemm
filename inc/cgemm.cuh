#ifndef CGEMM_CUH_
#define CGEMM_CUH_

#include <cuda.h>
#include <cuda_fp16.h>
#include <cublas_v2.h>
#include <vector>
#include <iostream>
#include <math.h>
#include <typeinfo>

#include "utils.cuh"

#define WARP_SIZE 32
#define N_WARPS 8

#define TC_K 16
#define TC_M 16
#define TC_N 16

template<typename InputType, typename OutputType>
__global__ void _cgemm_kernel_v1(const InputType* a, const InputType* b, OutputType* c, int k_size, int m_size, int n_size);

template<typename InputType, typename OutputType>
__global__ void _cgemm_kernel_v2(const InputType* a, const InputType* b, OutputType* c, int k_size, int m_size, int n_size);



template<typename InputType, typename OutputType>
class cgemm
{
public:
  typedef decltype(InputType::x) real;
  typedef decltype(InputType::y) imag;
  typedef decltype(OutputType::x) real_o;
  typedef decltype(OutputType::y) imag_o;
public:
  cgemm(int k, int m, int n, int version = 1);
  ~cgemm();
  void process();
  InputType* getA(){return a;}
  InputType* getB(){return b;}
  OutputType* getC(){return c;}
  void setA(InputType* p);
  void setB(InputType* p);
  void setC(OutputType* p);
  void klayout();
private:
  InputType* a;
  InputType* b;
  OutputType* c;
  cublasHandle_t blas_handle;
  int _version;
  int _k;
  int _m;
  int _n;
  cublasDataType_t dtype;
  real_o _alpha = 1;
  real_o _beta = 0;
  dim3 block;
  dim3 grid;
};




#include "src/dev_code.cu"
#include "src/cgemm.cu"

#endif
