#include <random>
#include <cmath>
#include "boost/program_options.hpp"
#include "inc/cgemm.cuh"
#include "inc/utils.cuh"

#define DEBUG 1



template<typename T>
void fill_matrix(T* mat, int row, int col)
{
  for(int i = 0; i < row; i++)
  {
    for(int ii = 0; ii < col; ii++)
    {
      // mat[i * col + ii].x = decltype(T::x)(rand() % 0x0f);
      // mat[i * col + ii].y = decltype(T::y)(rand() % 0x0f);
      mat[i * col + ii].x = i;//decltype(T::x)(rand() % 0x0f);
      mat[i * col + ii].y = 0;//decltype(T::y)(rand() % 0x0f);
    }
  }
}

void fill_matrix(__half2* mat, int row, int col)
{
  for(int i = 0; i < row; i++)
  {
    for(int ii = 0; ii < col; ii++)
    {
        mat[i * col + ii].x = __float2half((float)(rand() % 0x0f));
        mat[i * col + ii].y = __float2half((float)(rand() % 0x0f));
    }
  }
}


template<typename InputType, typename OutputType>
void gpu_process(InputType* h_a, InputType* h_b, OutputType* h_c, int k, int m, int n, int vers)
{
    cgemm<InputType, OutputType> proc(k,m,n,vers);
    std::cout << "GPU processing started .. " << std::flush;
    CUDA_ERROR_CHECK(cudaMemcpy((void*)proc.getA(), (void*)h_a, k*m*sizeof(InputType), cudaMemcpyHostToDevice));
    CUDA_ERROR_CHECK(cudaMemcpy((void*)proc.getB(), (void*)h_b, k*n*sizeof(InputType), cudaMemcpyHostToDevice));
    proc.process();
    CUDA_ERROR_CHECK(cudaDeviceSynchronize());
    CUDA_ERROR_CHECK(cudaMemcpy((void*)h_c, (void*)proc.getC(), m*n*sizeof(OutputType), cudaMemcpyDeviceToHost));
    std::cout << "done" <<std::endl;
}

template<typename InputType, typename OutputType>
void cpu_process(InputType* h_a, InputType* h_b, OutputType* h_c, int k_size, int m_size, int n_size, int vers)
{
  std::cout << "CPU processing started .. " << std::flush;
  for(int m = 0; m < m_size; m++)
  {
    for(int k = 0; k < k_size; k++)
    {
      for(int n = 0; n < n_size; n++)
      {
        h_c[m * n_size + n] = cmadd(h_a[m * k_size + k], h_b[k * n_size + n], h_c[m * n_size + n]);
      }
    }
  }
  std::cout << "done" <<std::endl;
}

template<typename T>
void compare(T* gpu, T* cpu, int m, int n)
{
  typedef decltype(T::x) real;
  real max_gpu=0;
  real max_cpu=0;
  real agpu_x, agpu_y, acpu_x, acpu_y;
  float tol;
  int err_cnt = 0;
  for(int i = 0; i < n*m; i++)
  {
    agpu_x = std::abs(gpu[i].x);
    agpu_y = std::abs(gpu[i].y);
    acpu_x = std::abs(cpu[i].x);
    acpu_y = std::abs(cpu[i].y);

    // For small numbers we need to accept a high tolerance (half to float)
    (acpu_x < 1 || acpu_y < 1) ? tol = 0.1 : tol = 0.01;

    if(  agpu_x > acpu_x + acpu_x*tol
      || agpu_x < acpu_x - acpu_x*tol
      || agpu_y > acpu_y + acpu_y*tol
      || agpu_y < acpu_y - acpu_y*tol )
    {
      err_cnt++;
#ifdef DEBUG
      if(err_cnt < 3)
      {
        std::cout << "Elements at position " << i << " are not equal\nGot "
        << gpu[i].x << " +i* " << gpu[i].y << " on GPU \nand "
        << cpu[i].x << " +i* " << cpu[i].y << " on CPU \n";
      }
    }
#endif
    if(std::abs(gpu[i].x) > max_gpu){
      max_gpu = gpu[i].x;
    }
    if(std::abs(gpu[i].y) > max_gpu){
      max_gpu = gpu[i].y;
    }
    if(std::abs(cpu[i].x) > max_cpu){
      max_cpu = cpu[i].x;
    }
    if(std::abs(cpu[i].y) > max_cpu){
      max_cpu = cpu[i].y;
    }
  }
#ifdef DEBUG
  std::cout << "Max gpu: " << max_gpu << "\nMax cpu: " << max_cpu << std::endl;
  std::cout << "Unequal elements: " << err_cnt << "/" << n*m <<std::endl;
#endif
  if(err_cnt > 0)
  {
    std::cout << "Test failed" << std::endl;
  }
  else
  {
    std::cout << "Test passed" << std::endl;
  }
}


template<typename InputType, typename OutputType>
void test(int k, int m, int n, int vers)
{
    // Allocate host memory
    InputType* h_a = new InputType[k*m];
    InputType* h_b = new InputType[k*n];
    OutputType* cpu_output = new OutputType[m*n];
    OutputType* gpu_output = new OutputType[m*n];
    // Fill input matrices
    fill_matrix(h_a, m, k);
    fill_matrix(h_b, k, n);
    gpu_process(h_a, h_b, gpu_output, k, m, n, vers);
    cpu_process(h_a, h_b, cpu_output, k, m, n, vers);
    compare<OutputType>(gpu_output, cpu_output, m, n);
}

int main(int argc, char** argv)
{
  int k, m, n, vers, dev_id;
  std::string i_type;
  std::string o_type;

  namespace po = boost::program_options;

  po::options_description desc("Options");

  desc.add_options()("help,h", "Print help message");
  desc.add_options()("k_size,k", po::value<int>(&k)->default_value(128), "Leading dimension of Matrix A (k x m)");
  desc.add_options()("n_size,n", po::value<int>(&n)->default_value(128), "Leading dimension of Matrix B (n x k)");
  desc.add_options()("m_size,m", po::value<int>(&m)->default_value(128), "Leading dimension of output Matrix C (n x m)");
  desc.add_options()("input_type,i", po::value<std::string>(&i_type)->default_value("char"), "Type of matrices A and B");
  desc.add_options()("output_type,o", po::value<std::string>(&o_type)->default_value("int"), "Type of matrix C");
  desc.add_options()("device_id,d", po::value<int>(&dev_id)->default_value(0), "ID of the Nvidia device");
  desc.add_options()("version,v", po::value<int>(&vers)->default_value(1), "Kernel version:\n  0=CUDA core cgemm\n  1=Tensor core cgemm\n  2 Tensor core cgemm with async copy");
  po::variables_map vm;
  try
  {
    po::store(po::parse_command_line(argc, argv, desc), vm);
    if(vm.count("help"))
    {
      std::cout << "Testing CGEMM -- test program for CGEMM" << std::endl
      << desc << std::endl;
      return 0;
    }
    po::notify(vm);
  }
  catch(po::error& e)
  {
    std::cerr << "ERROR: " << e.what() << std::endl;
    std::cerr << desc << std::endl;
    return 1;
  }
  CUDA_ERROR_CHECK(cudaSetDevice(dev_id));

  if(vers == 3){
    if(i_type == "float")
      test<cuComplex, cuComplex>(k, m, n, vers);
    else if(i_type == "char")
      test<char2, cuComplex>(k, m, n, vers);
  }
  else if(i_type == "char" && o_type == "int")
    test<char2, int2>(k, m, n, vers);
  else if(i_type == "half" && o_type == "float")
    test<__half2, float2>(k, m, n, vers);
  else
    std::cout << "Inserted types not implemented, doing nothing" << std::endl;


  return 0;
}
