#include <cmath>
#include <stdio.h>
#include <iostream>
#include <unistd.h>

#include "boost/program_options.hpp"
#include "inc/cgemm.cuh"
#include "inc/stats.cuh"

template<typename InputType, typename OutputType>
void profile(int k, int m, int n, int runs, int vers, std::string dir, std::string fname)
{
    unsigned long long complexity = (unsigned long long) 7*k*n*m;
    unsigned long long reads = (unsigned long long) k*m*sizeof(InputType) + k*n*sizeof(InputType);
    unsigned long long write = (unsigned long long) n*m*sizeof(OutputType);
    cgemm<InputType, OutputType> proc(k, m, n, vers);
    (fname == "") ? fname = "vers_" + std::to_string(vers) + ".csv": fname = fname + ".csv";
    KernelProfiler prof(
        fname,
        complexity,
        reads,
        write,
        (unsigned long long) k*m*sizeof(InputType)
    );
    std::cout << "------------------\nProfiling FFT Kernel started\n------------------\n" << std::endl;
    std::cout << "Problem size: " << k << " x " << m << " x " << n << std::endl;
    std::cout << "complexity:   " << complexity << std::endl;
    std::cout << "reads:        " << reads << std::endl;
    std::cout << "write:        " << write << std::endl;
    for(int i = 0; i < runs; i++)
    {
        prof.measure_start();
        proc.process();
        prof.measure_stop();
    }
    prof.finialize();
    prof.export_to_csv(dir + fname);
    std::cout << "\n------------------\nProfiling FFT Kernel stopped\n------------------" << std::endl;
    std::cout << "Results stored in " << dir << fname << std::endl;
}

int main(int argc, char**argv)
{
    int k, m, n, vers, runs;
    // int dev;
    std::string i_type;
    std::string o_type;
    std::string dir;
    std::string fname;

    namespace po = boost::program_options;
    po::options_description desc("Options");
    desc.add_options()("help,h", "Print help messages");
    desc.add_options()("k_size,k", po::value<int>(&k)->default_value(128), "Leading dimension of Matrix A (k x m)");
    desc.add_options()("n_size,n", po::value<int>(&n)->default_value(128), "Leading dimension of Matrix B (n x k)");
    desc.add_options()("m_size,m", po::value<int>(&m)->default_value(128), "Leading dimension of output Matrix C (n x m)");
    desc.add_options()("runs,r", po::value<int>(&runs)->default_value(2), "Runs");
    desc.add_options()("input_type,i", po::value<std::string>(&i_type)->default_value("char"), "Type of matrices A and B");
    desc.add_options()("output_type,o", po::value<std::string>(&o_type)->default_value("int"), "Type of matrix C");
    // desc.add_options()("device,d", po::value<int>(&dev)->default_value(0), "Device id");
    desc.add_options()("version,v", po::value<int>(&vers)->default_value(0), "Kernel version: 0 = cufftC2C; 0 < custom versions");
    desc.add_options()("dir", po::value<std::string>(&dir)->default_value("data/"), "Folder where to place .csv results");
    desc.add_options()("fname,f", po::value<std::string>(&fname)->default_value(""), "Output file name of .csv");
    po::variables_map vm;
    try
    {
        po::store(po::parse_command_line(argc, argv, desc), vm);
        if ( vm.count("help")  )
        {
            std::cout << "CGEMM profiling -- Profiles CGEMM-kernels in terms of execution time, throughput and memory bandwidth" << std::endl
            << desc << std::endl;
            return 0;
        }
        po::notify(vm);
    }
    catch(po::error& e)
    {
        std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
        std::cerr << desc << std::endl;
        return 1;
    }
    if(vers == 3){
      if(i_type == "float")
        profile<cuComplex, cuComplex>(k, m, n, runs, vers, dir, fname);
      else if(i_type == "char")
        profile<char2, cuComplex>(k, m, n, runs, vers, dir, fname);
    }
    else if(i_type == "char" && o_type == "int")
      profile<char2, int2>(k, m, n, runs, vers, dir, fname);
    else if(i_type == "half" && o_type == "float")
      profile<__half2, float2>(k, m, n, runs, vers, dir, fname);
    else
      std::cout << "Inserted types not implemented, doing nothing" << std::endl;
}
