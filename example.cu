#include "boost/program_options.hpp"
#include "inc/cgemm.cuh"

template<typename T, typename U>
void example(int k, int m, int n, int v)
{
  T* h_a = new T[k*m];
  T* h_b = new T[k*n];
  U* h_c = new U[m*n];

  for(int i = 0; i < k*m; i++){
    h_a[i].x = static_cast<decltype(T::x)>(1);//floor(i/k));
    h_a[i].y = static_cast<decltype(T::y)>(0);
  }
  for(int i = 0; i < k*n; i++){
    h_b[i].x = static_cast<decltype(T::x)>(1);//floor(i/n);
    h_b[i].y = static_cast<decltype(T::y)>(0);
  }

  cgemm<T, U> proc(k,m,n, v);

  CUDA_ERROR_CHECK(cudaMemcpy((void*)proc.getA(), (void*)h_a, k*m*sizeof(T), cudaMemcpyHostToDevice));
  CUDA_ERROR_CHECK(cudaMemcpy((void*)proc.getB(), (void*)h_b, k*n*sizeof(T), cudaMemcpyHostToDevice));
  // proc.klayout();
  proc.process();
  CUDA_ERROR_CHECK(cudaDeviceSynchronize());
  CUDA_ERROR_CHECK(cudaMemcpy((void*)h_c, (void*)proc.getC(), m*n*sizeof(U), cudaMemcpyDeviceToHost));

  for(int i = 0; i < m; i++)
  {
    for(int ii = 0; ii < n; ii++)
    {
      std::cout << "out[" << i << "][" << ii << "]=" << h_c[i * m + ii].x << " + i* " << h_c[i * m + ii].y << std::endl;
    }
  }
}

int main(int argc, char** argv)
{
  int k, m, n, vers;
  std::string i_type, o_type;

  namespace po = boost::program_options;

  po::options_description desc("Options");

  desc.add_options()("help,h", "Print help message");
  desc.add_options()("k_size,k", po::value<int>(&k)->default_value(32), "Leading dimension of Matrix A (k x m)");
  desc.add_options()("n_size,n", po::value<int>(&n)->default_value(32), "Leading dimension of Matrix B (n x k)");
  desc.add_options()("m_size,m", po::value<int>(&m)->default_value(32), "Leading dimension of output Matrix C (n x m)");
  desc.add_options()("input_type,i", po::value<std::string>(&i_type)->default_value("half"), "Type of matrices A and B");
  desc.add_options()("output_type,o", po::value<std::string>(&o_type)->default_value("float"), "Type of matrix C");
  desc.add_options()("version,v", po::value<int>(&vers)->default_value(1), "Leading dimension of output Matrix C (n x m)");
  po::variables_map vm;
  try
  {
    po::store(po::parse_command_line(argc, argv, desc), vm);
    if(vm.count("help"))
    {
      std::cout << "Example CGEMM -- Example program to run CGEMM" << std::endl
      << desc << std::endl;
      return 0;
    }
    po::notify(vm);
  }
  catch(po::error& e)
  {
    std::cerr << "ERROR: " << e.what() << std::endl;
    std::cerr << desc << std::endl;
    return 1;
  }
  // if(vers == 2)
  //   profile<float2, float2>(k, m, n, runs, vers, dir, fname);
  if(i_type == "char" && o_type == "int")
    example<char2, int2>(k, m, n, vers);
  else if(i_type == "half" && o_type == "float")
    example<__half2, float2>(k, m, n, vers);
  else
    std::cout << "Inserted types not implemented, doing nothing" << std::endl;

  return 0;
}
