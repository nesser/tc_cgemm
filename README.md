# Build & Compile
Go into project directory

```cd tc_cgemm```

Create build folder

```mkdir build```

Go into build folder and run cmake (CUDA architectures >= 75)

```cd build && cmake .. -DCMAKE_CUDA_FLAGS="--generate-code=arch=compute_80,code=[compute_80,sm_80]"```
