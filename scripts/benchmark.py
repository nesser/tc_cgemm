import numpy as np
import subprocess
import pandas as pd
import argparse
import os
from argparse import RawTextHelpFormatter

if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=RawTextHelpFormatter)
    parser.add_argument('--begin','-b', action='store', default=256, dest="start", help="Number of sizes to begin profiling (NxNxN)")
    parser.add_argument('--stop','-s', action='store', default=8192, dest="stop", help="Number of sizes to stop profiling (NxNxN)")
    parser.add_argument('--runs','-r', action='store', default=5, dest="runs", help="Number of runs per configuration")
    parser.add_argument('--device','-d', action='store', default=0, dest="device", help="CUDA device id of")
    parser.add_argument('--input_type','-i', action='store', default="half", dest="i_type", help="Input type of CGEMM")
    parser.add_argument('--output_type','-o', action='store', default="float", dest="o_type", help="Output type of CGEMM")
    parser.add_argument('--plot','-p', action='store', default=False, dest="plot", help="Plots the results")
    parser.add_argument('--dir', action='store', default="./data/", dest="dir", help="Directory to store results")
    parser.add_argument('--fname', '-f', action='store', default="", dest="fname", help="Name of output file (.csv)")
    parser.add_argument('--version', '-v', action='store', default=1, dest="mode", help="Kernel version: 0 = CUDA Core based kernel; 1-2 Tensor core based kernels")
    parser.add_argument('--exec', '-e', action='store', default="./build/profile", dest="exec", help="Executable file './profile'")

    start = int(parser.parse_args().start)
    stop = int(parser.parse_args().stop)
    runs = int(parser.parse_args().runs)
    device = int(parser.parse_args().device)
    plot = int(parser.parse_args().plot)
    fname = parser.parse_args().fname
    dir = parser.parse_args().dir
    mode = int(parser.parse_args().mode)
    i_type = parser.parse_args().i_type
    o_type = parser.parse_args().o_type

    sizes = np.arange(start, stop, start)
    elem = 256
    beam = 256
    chan = 16
    bw_ch = 2e9/chan
    t_p_s = sizes/chan * 1/(bw_ch)
    ih_bw = np.full(len(sizes), bw_ch * 2 * chan * elem * 2)/1e9
    ii_bw = np.full(len(sizes), bw_ch * 2 * chan * elem)/1e9

    # Profile kernels in subprocess
    exe = parser.parse_args().exec
    exe += " -r " + str(runs)
    exe += " -v " + str(mode)
    exe += " --dir " + dir
    exe += " -i " + i_type
    exe += " -o " + o_type
    if fname:
        exe += " -f " + str(fname)
    # print(exe)
    # for size in sizes:
    #     subprocess.call(exe + " -k " + str(elem) + " -m " + str(size) + " -n " + str(beam), shell=True)


    files = [f for f in os.listdir(dir) if '.csv' in f]
    if len(files) == 0:
        print("No csv-files found in " + dir)
        exit()
    else:
        print("Found files: " + str(files))
    avg_time = np.zeros((len(files), len(sizes)))
    avg_throughput = np.zeros((len(files), len(sizes)))
    avg_bandwidth = np.zeros((len(files), len(sizes)))
    kname = []
    for i, f in enumerate(files):
        table = pd.read_csv(dir + f, sep=",", engine='python')
        avg_time[i] = table['avg_time'].to_numpy()[:len(sizes)]
        avg_throughput[i] = table['avg_throughput'].to_numpy()[:len(sizes)]
        avg_bandwidth[i] = table['input_avg_bandwidth'].to_numpy()[:len(sizes)]
        dev_name = table["devicename"].to_numpy()[0]
        kname.append(table["kernelname"].to_numpy()[0])

    # Plot
    import matplotlib
    if not plot:
        matplotlib.use('Agg')
    import matplotlib.pyplot as plot

    fig = plot.figure(figsize=(12,8))
    for i in range(len(files)):
        plot.plot(sizes/chan, avg_time[i], label=files[i].split('.')[0])
        # if i == len(files)-1:
        #     ax1.plot(sizes/chan, t_p_s*1000, label="Required", linestyle='-.')
        plot.yscale('log')
        plot.title("Average time")
        plot.ylabel("Time [ms]")
        plot.xlabel("N -> [Samples] \nElements = {}\nBeams={}\nChannels={}".format(elem,beam,chan))
        plot.legend()
        fig.suptitle("GPU CGEMM Benchmarks on {} (runs = {})".format(dev_name, runs))
        plot.savefig('./img/time_' + dev_name + ".png")
        if plot:
            plot.show()
    fig = plot.figure(figsize=(12,8))
    for i in range(len(files)):
        plot.plot(sizes/chan, avg_throughput[i], label=files[i].split('.')[0])
        plot.title("Theoretical Throughput")
        plot.ylabel("Throughput [GOPS]")
        plot.xlabel("N -> [Samples] \nElements = {}\nBeams={}\nChannels={}".format(elem,beam,chan))
        plot.legend()
        fig.suptitle("GPU CGEMM Benchmarks on {} (runs = {})".format(dev_name, runs))
        plot.savefig('./img/throughput_' + dev_name + ".png")
        if plot:
            plot.show()

    fig = plot.figure(figsize=(12,8))
    for i in range(len(files)):
        plot.plot(sizes/chan, avg_bandwidth[i], label=files[i].split('.')[0])
        plot.title("Input Bandwidth")
        plot.ylabel("Bandwidth [GB/s]")
        plot.xlabel("N -> [Samples] \nElements = {}\nBeams={}\nChannels={}".format(elem,beam,chan))
        plot.legend()
        fig.suptitle("GPU CGEMM Benchmarks on {} (runs = {})".format(dev_name, runs))
        plot.savefig('./img/input_bw_' + dev_name + ".png")
        if plot:
            plot.show()
